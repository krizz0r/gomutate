package models

import (
	"io/ioutil"
	"log"
	"math"
	"os"
	"os/exec"
	"strconv"

	"../helper"
)

func (a *Analyser) GenerateMatrix() bool {
	magicmap := make(map[int]*PossibleMutation)
	i := int(math.Pow(float64(len(a.PossibleMutations)), 2) - 1)
	for c, m := range a.PossibleMutations {
		magicmap[(c + 1)] = m
	}
	for x := 1; x < (i + 1); x++ {
		a.Matrix[x] = make([]*Mutation, 0)
		for k, v := range magicmap {
			if x%k == 0 {
				a.Matrix[x] = append(a.Matrix[x], &Mutation{v, nil})
			}
		}
	}
	return true
}

func (a *Analyser) CreateMutations() error {
	tmpdir, err := ioutil.TempDir("", "mut")
	if err != nil {

		return err
	}
	os.Remove(tmpdir)
	// copy all the original files to the directory
	if err := helper.CopyDir(".", tmpdir); err != nil {
		return err
	}
	if len(a.Matrix) > 0 {
		// read the lines of the original source code
		lines, err := helper.ReadLines(a.Filename)
		if err != nil {
			return err

		}
		var copied = make([]string, len(lines))
		for n, mut := range a.Matrix {
			copy(copied, lines)
			dirname := strconv.Itoa(n)
			// first copy the file into a new directory
			// todo(@krizzle): don't copy the whole working dir
			if err := helper.CopyDir(tmpdir, dirname); err != nil {
				return err

			} else {
				// then change the lines that are stored in the matrix
				for _, change := range mut {
					copied[change.LineNumber-1] = change.MutatedLine
				}

				err = helper.WriteLines(copied, dirname+"/main.go")
				if err != nil {
					return err
				}

				err = helper.CopyFile(a.TestFile, dirname+"/"+a.TestFile)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func (a *Analyser) DoTheStuff() map[int]*MutationResult {
	returnslice := make(map[int]*MutationResult, 0)
	curPwd, _ := os.Getwd()

	for n, _ := range a.Matrix {
		os.Chdir(strconv.Itoa(n))
		log.Println("trying to change directory to", n)
		pwd, _ := os.Getwd()
		log.Println("current directory:", pwd)

		cmd := exec.Command("go", "test")
		if cmd != nil {
			//			cmd.Run()
			data, err := cmd.CombinedOutput()
			if err != nil && err.Error() != "exit status 1" {
				log.Println("am i a error?", err)
				return nil

			}
			log.Println(string(data))
			success := cmd.ProcessState.Success()
			returnslice[n] = &MutationResult{!success}
		}
		os.Chdir(curPwd)
	}
	return returnslice
}
