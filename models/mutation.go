package models

// this looks good
type MutatedFile struct {
	SourceFile, FullFilePath string
	PossibleMutations        []*PossibleMutation
	Matrix                   map[int][]*Mutation
}

func (ms *MutatedFile) AddPossibleMutation(original, mutated string, lineno int) {
	ms.PossibleMutations = append(ms.PossibleMutations, &PossibleMutation{original, mutated, lineno})
}

type PossibleMutation struct {
	OriginalLine, MutatedLine string
	LineNumber                int
}

type Mutation struct {
	*PossibleMutation
	Result *MutationResult
}

type MutationResult struct {
	Eliminated bool
}

type MutationScore struct {
	EliminatedMutants, LivingMutants, Score int
}
