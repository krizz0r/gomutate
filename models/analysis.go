package models

import (
	"bufio"
	"os"

	"../mutations/"
)

type Analyser struct {
	Filename, TestFile string
	MutatedFile
	MutationPerformers []mutations.Mutation
}

func CreateAnalyser(filename, testfile string) *Analyser {
	if _, err := os.Stat(testfile); err == nil {
		a := &Analyser{}
		a.Filename = filename
		a.TestFile = testfile
		a.MutationPerformers = append(a.MutationPerformers, &mutations.ConditionalMutation{})
		a.MutationPerformers = append(a.MutationPerformers, &mutations.EqualityInverseMutation{})
		a.Matrix = make(map[int][]*Mutation, 0)
		return a
	}
	return nil
}

func (a *Analyser) Analyse() error {
	inFile, _ := os.Open(a.Filename)
	defer inFile.Close()
	scanner := bufio.NewScanner(inFile)
	scanner.Split(bufio.ScanLines)
	lineno := 0 // this is not beautiful :[
	for scanner.Scan() {
		lineno++ // this is neither
		for _, performer := range a.MutationPerformers {
			if performer.IsMatch(scanner.Text()) {
				a.AddPossibleMutation(scanner.Text(), performer.Mutate(scanner.Text()), lineno)
			}
		}
	}
	return nil
}
