package mutations

import "strings"

type Mutation interface {
	IsMatch(line string) bool
	Mutate(line string) string
}

type ConditionalMutation struct {
}

func (cm ConditionalMutation) IsMatch(line string) bool {
	if !strings.Contains(line, ">=") {
		if strings.Contains(line, ">") {
			return true
		}
	}
	return false
}

func (cm ConditionalMutation) Mutate(line string) string {
	return strings.Replace(line, ">", "<", -1)
}

type EqualityInverseMutation struct {
}

func (cm EqualityInverseMutation) IsMatch(line string) bool {
	if strings.Contains(line, "==") {
		return true
	}
	return false
}

func (cm EqualityInverseMutation) Mutate(line string) string {
	return strings.Replace(line, "==", "!=", -1)
}
