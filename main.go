package main

import (
	"fmt"

	"./models"
)

func main() {
	a := models.CreateAnalyser("main.go", "main_test.go")
	if err := a.Analyse(); err != nil {
		panic(err)
	}
	a.GenerateMatrix()
	err := a.CreateMutations()
	if err != nil {
		panic(err)
	}

	results := a.DoTheStuff()

	for n, r := range results {
		fmt.Println(n, ":", r)
	}
}

func IsHigher(x, y int8) bool {
	if x > y {
		return true
	}
	return false
}

func IsLower(x, y int8) bool {
	if x < y {
		return true
	}
	return false
}

func IsNull(x int8) bool {
	if x == 0 {
		return true
	}
	return false
}

func SomePseudoMagicAction(foo, bar, z int8) bool {
	if foo > bar {
		if z != 0 {
			return true
		} else {
			return false
		}
	}
	return bar != 42
}
